import time
import telepot
from telepot.loop import MessageLoop
from telepot.namedtuple import InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardMarkup, KeyboardButton


def on_chat_message(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    if content_type == "text":
        command = msg['text']
    elif content_type == "contact":
        command = msg['contact']['phone_number']
        print command
    elif content_type == "location":
        command = msg['location']
    else:
        command = "nothing"

    if command == '/start':
        keyboard = InlineKeyboardMarkup(inline_keyboard=[
            [InlineKeyboardButton(text='Show Product', callback_data='show_product'), ],
            [InlineKeyboardButton(text='Send Your Contact !', callback_data='send_contact'), ],
            [InlineKeyboardButton(text='Contact', callback_data='contact'),
             InlineKeyboardButton(text='About', callback_data='about')],
        ])
        bot.sendMessage(chat_id, 'Welcome to Shopping Bot !')
        bot.sendPhoto(chat_id=chat_id,
                      photo="https://image.flaticon.com/icons/png/512/34/34627.png",
                      caption="Welcome to Shopping Bot",
                      reply_markup=keyboard)


def on_callback_query(msg):
    query_id, from_id, query_data = telepot.glance(msg, flavor='callback_query')
    # print('Callback Query:', query_id, from_id, query_data)

    if query_data == 'show_product':
        keyboard = InlineKeyboardMarkup(inline_keyboard=[
            [InlineKeyboardButton(text="Add to bag !", callback_data="add_to_bag"),
             InlineKeyboardButton(text="100 $", callback_data="product_price")],
            [InlineKeyboardButton(text="More Descriptions !", callback_data="more_about_product")]
        ])
        bot.sendPhoto(chat_id=from_id, photo=open('images/1.jpg'), caption="Product Name !",
                      reply_markup=keyboard)
        bot.answerCallbackQuery(query_id, text='Show Product!')

    elif query_data == "send_contact":
        keyboard = ReplyKeyboardMarkup(keyboard=[
            [KeyboardButton(text='Send Contact !', request_contact=True)],
        ], resize_keyboard=True, one_time_keyboard=True)
        bot.sendMessage(chat_id=from_id, text="Please send your Phone !", reply_markup=keyboard)

    elif query_data == "more_about_product":
        keyboard = InlineKeyboardMarkup(inline_keyboard=[
            [InlineKeyboardButton(text="Add to bag !", callback_data="add_to_bag"),
             InlineKeyboardButton(text="100 $", callback_data="product_price")]
        ])
        bot.sendPhoto(chat_id=from_id, photo=open('images/1.jpg'), caption="Product Name !")
        bot.sendMessage(chat_id=from_id,
                        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt"
                             " ut labore et dolore magna aliqua. Euismod lacinia at quis risus sed vulputate odio ut "
                             "enim. Aenean pharetra magna ac placerat. Amet purus gravida quis blandit turpis cursus in"
                             " hac. Aenean et tortor at risus viverra adipiscing at. Est placerat in egestas erat"
                             " imperdiet sed euismod nisi porta. Ultrices tincidunt arcu non sodales neque. Elementum"
                             " nibh tellus molestie nunc non. Viverra vitae congue eu consequat ac felis donec et odio."
                             " Integer quis auctor elit sed vulputate mi sit amet mauris.", reply_markup=keyboard)
        bot.answerCallbackQuery(query_id, text='More About Product!')

    elif query_data == 'about':
        bot.sendMessage(chat_id=from_id, text="About us!")

    elif query_data == 'contact':
        bot.sendMessage(chat_id=from_id, text="Contact us!")


TOKEN = "501697871:AAFgiKW6Lvm_2_uWi0TWGoVjp07rYuVEKFE"
bot = telepot.Bot(TOKEN)
MessageLoop(bot, {'chat': on_chat_message,
                  'callback_query': on_callback_query}).run_as_thread()
print('Listening ...')

while 1:
    time.sleep(10)
